
Drupal.behaviors.stackoverflowAdminSettingsForm = function () {
  $("#stackoverflow-admin-settings-form")
    .find(".stackoverflow-flair-mode-select input")
      .click(function () {
        if ($(this).val() == 'single_user') {
          $("#stackoverflow-admin-settings-form .stackoverflow-flair-user-ids").show("fast");
        }
        else {
          $("#stackoverflow-admin-settings-form .stackoverflow-flair-user-ids").hide("fast");
        }
      })
    .end()
};

