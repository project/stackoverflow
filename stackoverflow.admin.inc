<?php

/**
 * @file stackoverflow.admin.inc
 * Provides admin pages for the Stack Overflow module.
 */

/**
 * Stack Overflow admin settings form.
 */
function stackoverflow_admin_settings_form($form_state) {
  drupal_add_js(drupal_get_path('module', 'stackoverflow') . '/js/stackoverflow_admin_settings_form.js');
  $form = array();

  $form['stackoverflow_flair_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Flair mode'),
    '#description' => t('In single user mode, a single user ID will be used for each Stack Overflow-related site, and a Drupal block will be generated for each of them.'),
    '#options' => array(
      'disabled' => t('Flair disabled'),
      'single_user' => t('Single user mode'),
    ),
    '#default_value' => variable_get('stackoverflow_flair_mode', 'disabled'),
    '#attributes' => array('class' => 'stackoverflow-flair-mode-select'),
  );

  $form['user_ids'] = array(
    '#type' => 'fieldset',
    '#title' => t('User IDs'),
    '#attributes' => array(
      'class' => 'stackoverflow-flair-user-ids',
      // Hide the user ID settings if single user mode is not selected.
      'style' => (variable_get('stackoverflow_flair_mode', 'disabled') != 'single_user') ? 'display: none;' : '',
    ),
  );

  $form['user_ids']['stackoverflow_so_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Stack Overflow user id'),
    '#default_value' => variable_get('stackoverflow_so_user_id', NULL),
  );
    
  $form['user_ids']['stackoverflow_sf_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Server Fault user id'),
    '#default_value' => variable_get('stackoverflow_sf_user_id', NULL),
  );

  $form['user_ids']['stackoverflow_su_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('SuperUser user id'),
    '#default_value' => variable_get('stackoverflow_su_user_id', NULL),
  );

  $form['stackoverflow_flair_cache_duration'] = array(
    '#type' => 'select',
    '#title' => t('Flair cache duration'),
    '#description' => t('Select how long the flair data should be cached.'),
    '#options' => array(
      1800 => t('30 minutes'),
      3600 => t('An hour'),
      7200 => t('Two hours'),
      14400 => t('Four hours'),
      43200 => t('12 hours'),
      86400 => t('24 hours'),
    ),
    '#default_value' => variable_get('stackoverflow_flair_cache_duration', 43200),
  );

  return system_settings_form($form);
}

/**
 * Validation for admin settings form.
 */
function stackoverflow_admin_settings_form_validate($form, &$form_state) {
  // Make sure that each (non-empty) user ID is a number.
  foreach (array('stackoverflow_so_user_id', 'stackoverflow_sf_user_id', 'stackoverflow_su_user_id') as $name) {
    $value = trim($form_state['values'][$name]);
    if (!empty($form_state['values'][$name]) || $value === '0') {
      if (intval($value < 1)) {
        // If value is not valid, store it as NULL.
        form_set_value($form['user_ids'][$name], NULL, $form_state);
        // Only display validation errors in single user mode 
        // (otherwise, the fieldset is not shown).
        if ($form_state['values']['stackoverflow_flair_mode'] == 'single_user') {
          form_error($form['user_ids'][$name], t('User ID should be a number larger than 0.'));
        }
      }
      else {
        // Cast the value to an integer before storing it.
        form_set_value($form['user_ids'][$name], intval($value), $form_state);
      }
    }
  }
}

