<?php

/**
 * @file stackoverflow.theme.inc
 * Theme system implementation for the Stack Overflow module.
 */

/**
 * Variable preprocessing for the stackoverflow_flair template.
 */
function template_preprocess_stackoverflow_flair(&$variables) {
  $variables['site_url'] = constant('STACKOVERFLOW_' . strtoupper($variables['site']) . '_URL');

  $site_names = array(
    'so' => t('Stack Overflow'),
    'sf' => t('Server Fault'),
    'su' => t('SuperUser'),
  );

  $variables['site_name'] = $site_names[$variables['site']];
}

