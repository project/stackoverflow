<?php

/**
 * @file stackoverflow_flair.tpl.php
 * Template for rendering the Stack Overflow site-family flair.
 */
?>
<div class="valuable-flair <?php print $site; ?> clear-block">
  <div class="gravatar">
    <a title="See my profile on <?php print $site_name; ?>" href="<?php print $flair_data->profileUrl; ?>"><?php print $flair_data->gravatarHtml; ?></a>
  </div>
  <div class="userInfo">
    <span class="username">
      <img src="<?php print $site_url; ?>favicon.ico" />
      <a title="See my profile on <?php print $site_name; ?>" href="<?php print $flair_data->profileUrl; ?>"><?php print $flair_data->displayName; ?></a>
    </span>
    <br />
    <span class="reputation-score" title="reputation score"><?php print $flair_data->reputation; ?></span>
    <br />
    <?php print $flair_data->badgeHtml; ?>
  </div>
</div>

